import React from "react";

function App() {
  return (
    <div>
      <h1>Hello, World!</h1>
      <p>This is a simple JSX example.</p>
    </div>
  );
}

export default App;
